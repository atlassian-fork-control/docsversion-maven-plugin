/*
 * Copyright © 2011 Atlassian Pty Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.maven.plugins.docsversion

import org.apache.maven.project.MavenProject
import org.apache.maven.project.MavenProjectHelper
import org.codehaus.gmaven.mojo.GroovyMojo

import static com.atlassian.maven.plugins.docsversion.DocsVersionGenerator.getDocsVersion

/**
 * set-property MOJO.
 *
 * @goal set-property
 */
class SetPropertyMojo extends GroovyMojo
{
  /**
   * The name of the property to set.
   *
   * @parameter default-value="docs.version"
   */
  String property

  /**
   * The optional property name to be read, defaults to 'project.version'
   *
   * @parameter
   */
  String versionProperty

  /**
   * @parameter expression="${project}"
   * @required
   * @readonly
   */
  MavenProject project

  /**
   * The Maven project helper.
   *
   * @component
   */
  MavenProjectHelper projectHelper

  /**
   * Sets the property in the project.
   */
  void execute()
  {
    def propertiesMap = project.properties
    def versionString = determineVersionString(propertiesMap)

    log.info "Parsing '${versionString}' to determine docs version"
    def docsVersion = getDocsVersion(versionString)

    log.info "Setting project property ${property}=${docsVersion}"
    propertiesMap[property] = docsVersion
  }

  private String determineVersionString(Properties propertiesMap)
  {
    if (versionProperty != null)
    {
      log.info "Using property '${versionProperty}' to determine docs version"
      if (propertiesMap.containsKey(versionProperty))
      {
        return (String) propertiesMap[versionProperty]
      }
      else
      {
        throw new NoSuchElementException("Configured property not defined: ${versionProperty}".toString())
      }
    }
    else
    {
      log.info "Using project version to determine docs version"
      return project.version
    }
  }
}
