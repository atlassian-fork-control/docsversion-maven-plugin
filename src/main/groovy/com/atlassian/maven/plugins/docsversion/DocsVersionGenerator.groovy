package com.atlassian.maven.plugins.docsversion

/**
 * Calculates the docs.version from a Maven project version.
 */
class DocsVersionGenerator {
  /**
   * Returns the docs version for this project.
   *
   */
  static String getDocsVersion(String projectVersion)
  {
    def version = projectVersion.minus('-SNAPSHOT').tokenize('.')
    def majorVersion = version[0]
    def minorVersion = version[1].tokenize('-')[0] // discard qualifiers

    // produces '052' for 5.2-SNAPSHOT, etc
    return String.format('%02d%s', Integer.parseInt(majorVersion), minorVersion)
  }
}
