package com.atlassian.maven.plugins.docsversion

import org.junit.Test

import static com.atlassian.maven.plugins.docsversion.DocsVersionGenerator.getDocsVersion
import static junit.framework.Assert.assertEquals

class DocsVersionGeneratorTest
{
  @Test
  void majorRelease()
  {
    assertEquals("052", getDocsVersion("5.2"))
    assertEquals("052", getDocsVersion("5.2-SNAPSHOT"))
  }

  @Test
  void bugfixRelease()
  {
    assertEquals("052", getDocsVersion("5.2.1"))
    assertEquals("052", getDocsVersion("5.2.1-SNAPSHOT"))
  }

  @Test
  void kickassRelease()
  {
    assertEquals("052", getDocsVersion("5.2.KA9"))
    assertEquals("052", getDocsVersion("5.2.1-SNAPSHOT"))
  }

  @Test
  void milestoneRelease()
  {
    assertEquals("052", getDocsVersion("5.2-m01"))
    assertEquals("052", getDocsVersion("5.2-m06_1"))
    assertEquals("052", getDocsVersion("5.2-m06.1"))
  }

  @Test
  void buildsWithQualifiers()
  {
    assertEquals("050", getDocsVersion("5.0.6-rc1"))
    assertEquals("051", getDocsVersion("5.1-JRADEV-11044"))
    assertEquals("052", getDocsVersion("5.2-rotp"))
    assertEquals("052", getDocsVersion("5.2-webhooks-5"))
    assertEquals("052", getDocsVersion("5.2-testingit-SNAPSHOT"))
    assertEquals("052", getDocsVersion("5.2.1-testingit-SNAPSHOT"))
  }
}
